var app = new Vue({
  el: '#app',
  beforeMount() {
    this.testThemeCookie()
  },
  created(){
    this.handleScroll()
  },
  data: {
    search: '',
    blindtests: object,
    darkTheme: true,
    matchOnly: false,
    selected: "song",
    searchTypes: [{
      "key": "song",
      "text": "les pistes"
    }, {
      "key": "theme",
      "text": "les thèmes"
    }],
    selectedBlindtest: "",
    themeExpanded: null
  },
  methods: {
    handleScroll: function(){
      window.onscroll = function () {
        var searchContainer = document.getElementById('searchContainer');
          if (window.scrollY >= 100 ) {
            searchContainer.classList.add("scrolled-search");
          }
          else {
            searchContainer.classList.remove("scrolled-search");
          }
      };
    },
    getCookie: function(name) {
      var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
      return v ? v[2] : null;
    },

    setCookie: function(name, value, days) {
      var d = new Date;
      d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
      document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString() + ";SameSite=Lax";
    },

    testThemeCookie: function() {
      if (this.getCookie("theme") != "dark") {
        document.querySelector("html").dataset.theme = "light"
        this.darkTheme = false
      }
    },

    expand: function(theme) {
      this.themeExpanded = (theme != this.themeExpanded) ?
        theme :
        null
    },

    switchTheme: function() {
      if (!this.darkTheme) {
        document.querySelector("html").dataset.theme = "dark"
        this.setCookie("theme", "dark", 30)
      } else {
        document.querySelector("html").dataset.theme = "light"
        this.setCookie("theme", "color", 30)
      }
    },

    testRichard: function() {
      if (this.search.toLowerCase() == "richard anthony") {
        this.richardized()
      }
    },

    richardized: function() {
      document.querySelector("#richard").remove()
      document.querySelector("body").classList.add("richardized")
    },

    rotate: function() {
      const root = document.documentElement,
        gRoot = getComputedStyle(root)

      var rotation = parseInt(gRoot.getPropertyValue('--turn'))
      rotation = (rotation + 6) % 360
      if (rotation == 0) {
        this.richardized()
      }
      root.style.setProperty('--turn', rotation + "deg")
    },

    reset: function() {
      this.search = ''
      this.matchOnly = false
      this.selected = "song"
      this.selectedBlindtest = ""
      this.themeExpanded = null

      return;
    },

    getSearch: function() {
      return this.search.toLowerCase()
    },

    getFullName: function(song, theme) {
      str = (theme.type != "double") ?
        song.fullname :
        song.info[0].fullname + song.info[1].fullname;

      return str.toLowerCase();
    },

    songMatch: function(song, theme) {
      return this.getFullName(song, theme).includes(this.getSearch())
    },

    themeMatch: function() {
      return theme.title.toLowerCase().includes(this.getSearch())
    },

    testTheme: function(theme) {
      return (this.selected == "song") ?
        theme.songs.some(song => this.songMatch(song, theme)) :
        theme.title.toLowerCase().includes(this.getSearch())
    },

    testBlindtest: function(blindtest) {
      return (this.selected == "song") ?
        blindtest.themes.some(theme => theme.songs.some(song => this.songMatch(song, theme))) :
        blindtest.themes.some(theme => this.testTheme(theme))
    },

    testSong: function(song, theme) {
      return this.search != "" && this.songMatch(song, theme);
    },

    displaySong: function(song, theme) {
      return (this.selected == 'theme' || !this.matchOnly || (this.matchOnly && this.testSong(song, theme)) || this.themeExpanded == theme)
    },

    displayBlindtest: function(blindtest) {
      return (!this.selectedBlindtest && this.testBlindtest(blindtest)) || (blindtest.name == this.selectedBlindtest)
    }
  }
})
